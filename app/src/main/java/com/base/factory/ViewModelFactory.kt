package com.base.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.`fun`.MainViewModel

class ViewModelFactory(context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
            when{
                isAssignableFrom(MainViewModel::class.java) -> {
                    MainViewModel()
                }
                else -> throw IllegalStateException("unknown view model: $modelClass")
            }

        } as T
    companion object{
        fun getInstance(activity : Context) : ViewModelFactory = ViewModelFactory(activity)
    }
}
