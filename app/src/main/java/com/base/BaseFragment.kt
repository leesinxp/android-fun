package com.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseFragment<VM : BaseViewModel<Any?>> : Fragment(){
    private lateinit var intervalViewModel : VM
    val viewModel: VM get() = intervalViewModel
    abstract fun onCreateViewModel(): VM
    private val messageSubscription by lazy { CompositeDisposable() }

    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun initView()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intervalViewModel = onCreateViewModel()
        initView()
    }


}
