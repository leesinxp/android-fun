package com.base

import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity< VM : BaseViewModel<Any?>> : AppCompatActivity(){

    private lateinit var internalViewModel: VM
    val viewModel: VM get() = internalViewModel
    abstract fun onCreateViewModel(): VM
//    private val messageSubscription by lazy { CompositeDisposable() }

    @LayoutRes
    abstract fun layoutId(): Int
    abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        internalViewModel = onCreateViewModel()
        initView()
    }
}
