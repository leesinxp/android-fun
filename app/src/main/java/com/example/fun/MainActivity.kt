package com.example.`fun`

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.base.BaseActivity
import com.base.etx.initViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel>() {
    private var isShow = false
    override fun onCreateViewModel(): MainViewModel = initViewModel()
    override fun layoutId(): Int = R.layout.activity_main
    override fun initView() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        icon_show_comic.setOnClickListener {
            isShow =! isShow
            Visible()
        }
    }
    private fun Visible(){
        if (isShow){
           icon_show_comic.setImageDrawable(resources.getDrawable(R.drawable.ic_line2))
           listComic.visibility = View.VISIBLE
        }else{
            icon_show_comic.setImageDrawable(resources.getDrawable(R.drawable.ic_line))
            listComic.visibility = View.GONE
        }
    }
}