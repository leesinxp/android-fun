package com.example.`fun`

import android.content.Intent
import android.os.Handler
import android.view.WindowManager
import com.base.BaseActivity
import com.base.etx.initViewModel

class SplashActivity : BaseActivity<MainViewModel>() {
    private val SPLASH_SCREEN = 1500
    override fun onCreateViewModel(): MainViewModel = initViewModel()
    override fun layoutId(): Int = R.layout.splash_activity
    override fun initView() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, SPLASH_SCREEN.toLong())
    }

}